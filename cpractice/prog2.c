#include <stdio.h>
/*Q1: D. int *x; 
**Q2: C. &a;
**Q3: A. a;


*/
void main() {
	
	int a = 5;
	int *x = &a;//Q2
	printf("a holds %d \n",a);
	printf("value stored at address %8x is %d\n",x, *x);//Q1, Q3
}

#include <stdio.h>
/*
Q1: B. b.var
Q2: A. b->var
Q3: D. struct a_struct {int a;};
Q4: B. struct foo var;
*/
struct myStruct{//Q3
	int var;
};
void main() {


	
	struct myStruct b;//Q4
	struct myStruct *bp = &b;
	b.var = 5;
	printf("b.var = %d \n", b.var);//Q1
	printf("bp->var = %d \n", bp->var);//Q2
}

OBJECTS = loader.o kmain.o
CC = gcc
CFLAGS = -m32 -nostdlib -nostdinc -fno-builtin -fno-stack-protector \
	-nostartfiles -nodefaultlibs -Wall -Wextra -Werror -c
LDFLAGS = -T link.ld -melf_i386
AS = nasm
ASFLAGS = -f elf



all: kernel.elf

kernel.elf: $(OBJECTS) iso/boot/grub link.ld
	ld $(LDFLAGS) $(OBJECTS) -o kernel.elf
	
iso/boot/grub: 
	mkdir -p iso/boot/grub
	

%.o: %.s
	$(AS) $(ASFLAGS) $< -o $@
	
%.o: %.c
	$(CC) $(CFLAGS) $< -o $@
	
run: os.iso bochsrc.txt
	bochs -f bochsrc.txt -q
	
os.iso: kernel.elf grub.cfg
	cp kernel.elf iso/boot/
	cp grub.cfg iso/boot/grub
	grub-mkrescue -o os.iso iso
	
clean: 
	rm -rf iso *- kernel.elf os.iso $(OBJECTS) bochslog.txt
